﻿using System;

namespace ProjectGil.Common.Models {
    public class GpsLocation {
        public double? Altitude { get; set; }
        public double? Course { get; set; }
        public double? Speed { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
    }
}
