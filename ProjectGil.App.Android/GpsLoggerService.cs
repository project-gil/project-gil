﻿using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using ProjectGil.App.Services;
using Xamarin.Forms;

namespace ProjectGil.App.Droid {
    [Service]
    public class GpsLoggerService : Service {
        CancellationTokenSource _cts;
        public override IBinder OnBind(Intent intent) {
            return null;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId) {
            _cts = new CancellationTokenSource();

            _ = Task.Run(async () => {
                try {
                    //INVOKE THE SHARED CODE
                    var counter = new GpsService();
                    await counter.PublishGpsSignals(1000, _cts.Token).ConfigureAwait(false);
                } catch (System.OperationCanceledException) {
                } finally {
                    if (_cts.IsCancellationRequested) {
                        Device.BeginInvokeOnMainThread(
                            () => MessagingCenter.Send("Background task was cancelled", "CancelledMessage")
                        );
                    }
                }

            }, _cts.Token);

            return StartCommandResult.Sticky;
        }

        public override void OnDestroy() {
            if (_cts != null) {
                _cts.Token.ThrowIfCancellationRequested();

                _cts.Cancel();
            }
            base.OnDestroy();
        }
    }
}