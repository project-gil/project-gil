﻿using System;
using System.Globalization;
using System.Net;
using ProjectGil.App.Services;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using RestSharp;
using Xamarin.Essentials;
using Xunit;

namespace ProjectGil.App.UnitTest
{
    public class GpsServiceTest
    {
        private readonly Mock<RestClient> _restClient = new Mock<RestClient>();
        private readonly Mock<IGeoLocation> _geoLocation = new Mock<IGeoLocation>();
        private readonly IGpsService _gpsService;
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();

        public GpsServiceTest()
        {
            _gpsService = new GpsService(_restClient.Object, _geoLocation.Object);
        }

        [Fact]
        public void TestCancellingTokenStopsService()
        {
            //Setup required mocks
            _geoLocation.Setup(x => x.GetLocationAsync(It.IsAny<GeolocationRequest>(), _cts.Token)).Returns(Task.FromResult(new Location()));
            _restClient.Setup(x => x.ExecuteTaskAsync(It.IsAny<RestRequest>(), _cts.Token)).Returns(Task.FromResult((IRestResponse)new RestResponse{ StatusCode = HttpStatusCode.OK}));

            var task = _gpsService.PublishGpsSignals(1000, _cts.Token);
            Assert.False(task.IsCompleted);
            _cts.Cancel();
            Assert.True(task.IsCompleted);
            Assert.True(task.IsCanceled);
        }

        [Fact]
        public void TestServicePublishesTheRightLocation()
        {
            var location = new Location(234.42234, 235453.34, DateTimeOffset.Now) {
                Altitude = 2342.324,
                Course = 32425234,
                Speed = 942.23
            };

            //Setup required mocks
            _geoLocation.Setup(x => x.GetLocationAsync(It.IsAny<GeolocationRequest>(), It.IsAny<CancellationToken>())).Returns(Task.FromResult(location));
            //Make sure the status code is not Ok so this does not run again. By default it is 0.
            _restClient.Setup(x => x.ExecuteTaskAsync(It.IsAny<RestRequest>(), It.IsAny<CancellationToken>())).Returns(Task.FromResult((IRestResponse)new RestResponse()));

            var task = _gpsService.PublishGpsSignals(1000, _cts.Token);
            //This verification is pretty gross, however I do not have the data to do it nicer, this is just checking that the request body contains all of the information from the location api.
            _restClient.Verify(
                x => x.ExecuteTaskAsync(
                    It.Is<RestRequest>(y =>
                        y.Parameters.Exists(z =>
                            z.Type == ParameterType.RequestBody &&
                            z.ToString().Contains(location.Latitude.ToString(CultureInfo.InvariantCulture)) &&
                            z.ToString().Contains(location.Longitude.ToString(CultureInfo.InvariantCulture)) &&
                            z.ToString().Contains(location.Speed.Value.ToString(CultureInfo.InvariantCulture)) &&
                            z.ToString().Contains(location.Altitude.Value.ToString(CultureInfo.InvariantCulture)) &&
                            z.ToString().Contains(location.Course.Value.ToString(CultureInfo.InvariantCulture))
                        ) && y.Parameters.Count == 1), _cts.Token),
                Times.Once);
        }

        [Theory]
        [InlineData(HttpStatusCode.BadRequest)]
        [InlineData(HttpStatusCode.BadGateway)]
        [InlineData(HttpStatusCode.InternalServerError)]
        [InlineData(HttpStatusCode.Conflict)]
        [InlineData(HttpStatusCode.Forbidden)]
        [InlineData(HttpStatusCode.Unauthorized)]
        [InlineData(HttpStatusCode.Redirect)]
        [InlineData(HttpStatusCode.GatewayTimeout)]
        public void TestUnsuccessfulResponseStopsTask(HttpStatusCode returnCode)
        {
            //Setup required mocks
            _geoLocation.Setup(x => x.GetLocationAsync(It.IsAny<GeolocationRequest>(), _cts.Token)).Returns(Task.FromResult(new Location()));
            _restClient.Setup(x => x.ExecuteTaskAsync(It.IsAny<RestRequest>(), _cts.Token)).Returns(Task.FromResult((IRestResponse)new RestResponse{ StatusCode = returnCode}));

            var task = _gpsService.PublishGpsSignals(1000, _cts.Token);
            //Sleep the thread to ensure publish was called.
            Thread.Sleep(100);
            Assert.True(task.IsCompleted);
            Assert.False(task.IsCanceled);
        }

        [Fact]
        public void SanityTestOnDefaultConstructor() {
            IGpsService service = new GpsService();
            Assert.NotNull(service);
        }
    }
}
