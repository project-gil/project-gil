#r "paket:
nuget Fake.IO.FileSystem
nuget Fake.DotNet.MSBuild
nuget Fake.BuildServer.GitLab
nuget Fake.BuildServer.AppVeyor
nuget Fake.DotNet.Xamarin
nuget Fake.DotNet.Cli
nuget Fake.Testing.ReportGenerator
nuget Fake.Core.Target //"
#load "./.fake/build.fsx/intellisense.fsx"

open Fake.Core
open Fake.IO
open Fake.IO.Globbing.Operators
open Fake.DotNet
open Fake.BuildServer
open Fake.Testing


BuildServer.install [
    GitLab.Installer
    AppVeyor.Installer
]

// Default target
Target.create "Default" (fun _ ->
  Trace.trace "Hello World from FAKE"
)

Target.create "Clean" (fun _ -> 
    !! "*/bin"
    ++ "*/obj"
    -- "*Android/bin"
    -- "*Andriod/obj"
    |> Shell.cleanDirs
)

Target.create "Build" (fun _ -> 
    !! "*/*.csproj"
    -- "*/*.Android.csproj"
    |> Seq.iter (DotNet.build (fun o -> {o with Configuration=DotNet.BuildConfiguration.Release }))
)

Target.create "Test" (fun _ -> 
    !! "*/*Test.csproj"
    -- "*/*Web.UITest.csproj"
    |> Seq.iter (fun x -> 
        let path = 
            x.Replace('\\', '/').Split('/')

        let testPath = 
            path
            |> Array.skip (path.Length - 2)
            |> Array.take 1
            |> Array.reduce (fun curr next -> curr+"/"+next)
            |> (fun x -> "./test/" + x)

        let arguments = 
            { MSBuild.CliArguments.Create () with 
                Properties = [
                    ("CollectCoverage","true")
                    ("CoverletOutput","../test/CoverageResults/")
                    ("CoverletOutputFormat","json,opencover")
                    ("MergeWith","../test/CoverageResults/coverage.json")
                ]
            }

        DotNet.test (fun t -> {t with Logger = Some "JUnit"; ResultsDirectory = Some testPath; MSBuildParams = arguments}) x
        Trace.publish ImportData.Junit (testPath+"/TestResults.xml")
    )
)


Target.create "Clean-Andriod" (fun _ -> 
    !! "*Android/bin"
    ++ "*Andriod/obj"
    |> Shell.cleanDirs
)

Target.create "Build-Andriod" (fun _ -> 
    let andriodParams = 
        { Xamarin.AndroidPackageDefaults with 
            ProjectPath = "ProjectGil.App.Android/ProjectGil.App.Android.csproj"
            OutputPath = "ProjectGil.App.Android/bin/Release"
        }
    Xamarin.AndroidBuildPackages (fun _ -> andriodParams)
    |> List.iter (fun f -> Trace.publish (ImportData.BuildArtifactWithName "Andriod APK") (f.DirectoryName+"\\"+f.Name))
)

open Fake.Core.TargetOperators

"Clean-Andriod"
  ==> "Build-Andriod"

// start build
Target.runOrDefault "Test"