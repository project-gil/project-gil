﻿using Moq;
using ProjectGil.Common.Models;
using ProjectGIL.Web.Controllers;
using ProjectGIL.Web.Services;
using Xunit;

namespace ProjectGil.Web.UnitTest.ControllerTest {
    public class GpsControllerTest {
        private Mock<IGpsStore> store = new Mock<IGpsStore>();
        private GpsController controller;

        public GpsControllerTest() {
            controller = new GpsController(store.Object);
        }

        [Fact]
        public void TestSubmitAddsToStore() {
            var item = new GpsLocation();
            var id = "test";
            
            var result = controller.SubmitGps("test", item);
            store.Verify(s => s.Add(id, item), Times.Once);
        }

        [Fact]
        public void TestGettingItemFromStore() {
            var id = "test";
            var result = controller.GetLatest("test");
            store.Verify(s => s.GetLatest(id), Times.Once);
        }

        [Fact]
        public void TestGetsAreSourcedFromStore() {
            var item = new GpsLocation();
            var id = "test";

            store.Setup(x => x.GetLatest(id)).Returns(item);
            var result = controller.GetLatest(id);
            Assert.Equal(item, result.Value);
        }
    }
}
