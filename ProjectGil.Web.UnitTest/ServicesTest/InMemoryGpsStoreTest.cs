﻿using ProjectGil.Common.Models;
using ProjectGIL.Web.Services;
using System;
using Xunit;

namespace ProjectGil.Web.UnitTest.ServicesTest {
    public class InMemoryGpsStoreTest : IDisposable {
        InMemoryGpsStore store;

        public InMemoryGpsStoreTest() {
            store = new InMemoryGpsStore();
        }

        public void Dispose() {}

        [Fact]
        public void TestAddingLocationReallyAdds() {
            var testItem = new GpsLocation {
                Altitude = null,
                Course = null,
                Speed = null,
                Latitude = 0,
                Longitude = 3,
                TimeStamp = DateTimeOffset.UtcNow
            };

            store.Add("test", testItem);
            var item = store.GetLatest("test");
            Assert.Equal(testItem, item);
        }

        [Fact]
        public void TestStoreIsEmptyByDefault() {
            Assert.Null(store.GetLatest("test"));
        }

        [Fact]
        public void TestLastestLocationIsLatest() {
            var testItem = new GpsLocation();
            var testItem2 = new GpsLocation();

            store.Add("test", testItem);
            store.Add("test", testItem2);
            var item = store.GetLatest("test");
            Assert.Equal(testItem2, item);
        }

        [Fact]
        public void TestDataFromOneUserDoesNotEffectAnother() {
            var testItem = new GpsLocation();
            var testItem2 = new GpsLocation();

            store.Add("test", testItem);
            store.Add("test2", testItem2);
            var item = store.GetLatest("test");
            var item2 = store.GetLatest("test2");
            Assert.Equal(testItem, item);
            Assert.Equal(testItem2, item2);
            Assert.Null(store.GetLatest("test3"));
        }
    }
}
