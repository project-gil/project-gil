﻿using Microsoft.AspNetCore.Mvc;
using ProjectGil.Common.Models;
using ProjectGIL.Web.Services;

namespace ProjectGIL.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GpsController : ControllerBase {
        private IGpsStore _gpsStore;

        public GpsController(IGpsStore gpsStore) {
            _gpsStore = gpsStore;
        }

        [HttpPost("{id}")]
        public IActionResult SubmitGps(string id, [FromBody] GpsLocation gpsLocation) {
            _gpsStore.Add(id, gpsLocation);
            return Ok();
        }

        [HttpGet("{id}")]
        public ActionResult<GpsLocation> GetLatest(string id) {
            var location = _gpsStore.GetLatest(id);
            if (location == null)
                return NotFound();

            return location;
        }

   }
}