﻿
using ProjectGil.Common.Models;

namespace ProjectGIL.Web.Services {
    public interface IGpsStore {
        void Add(string userId, GpsLocation location);
        GpsLocation GetLatest(string userId);
    }
}
