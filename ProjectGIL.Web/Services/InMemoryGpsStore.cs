﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectGil.Common.Models;

namespace ProjectGIL.Web.Services {
    public class InMemoryGpsStore : IGpsStore {
        private Dictionary<string, GpsLocation> _store = new Dictionary<string, GpsLocation>();

        public void Add(string userId, GpsLocation location) {
            if (_store.ContainsKey(userId))
                _store[userId] = location;
            else
                _store.Add(userId, location);
        }

        public GpsLocation GetLatest(string userId) {
            return _store.GetValueOrDefault(userId);
        }
    }
}
