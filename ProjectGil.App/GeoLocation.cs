﻿using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace ProjectGil.App {
    public interface IGeoLocation {
        Task<Location> GetLocationAsync();
        Task<Location> GetLocationAsync(GeolocationRequest request, CancellationToken cancellationToken);
        Task<Location> GetLocationAsync(GeolocationRequest request);
    }

    public class GeoLocation : IGeoLocation {
        public Task<Location> GetLocationAsync() => Geolocation.GetLocationAsync();
        public Task<Location> GetLocationAsync(GeolocationRequest request, CancellationToken cancellationToken) => Geolocation.GetLocationAsync(request, cancellationToken);
        public Task<Location> GetLocationAsync(GeolocationRequest request) => Geolocation.GetLocationAsync(request);
    }
}
