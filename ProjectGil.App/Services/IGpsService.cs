﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectGil.App.Services {
    public interface IGpsService {
        Task PublishGpsSignals(int delay, CancellationToken token);
    }
}
