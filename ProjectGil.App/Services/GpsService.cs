﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ProjectGil.Common.Models;
using Xamarin.Essentials;
using RestSharp;

namespace ProjectGil.App.Services {
    public class GpsService : IGpsService
    {
        private readonly RestClient _client;
        private readonly IGeoLocation _geoLocation;

        public GpsService() {
            _client = new RestClient(App.AzureBackendUrl);
            _geoLocation = new GeoLocation();
        }


        /// <summary>
        /// USED ONLY FOR UNIT TESTING
        /// Does not function
        /// </summary>
        /// <param name="client">The mocked rest client</param>
        /// <param name="geoLocation">Mocked geolocation service</param>
        public GpsService(RestClient client, IGeoLocation geoLocation)
        {
            _client = client;
            _geoLocation = geoLocation;
        }

        /// <summary>
        /// While the token is not cancelled publish GPS location.
        /// This function will run until the cancellation token is canceled. 
        /// </summary>
        /// <param name="delay">The delay between location probes</param>
        /// <param name="cancellationToken">The token to cancel the request.</param>
        public async Task PublishGpsSignals(int delay, CancellationToken cancellationToken) {
            while (!cancellationToken.IsCancellationRequested) {
                var location = await _geoLocation.GetLocationAsync(new GeolocationRequest(GeolocationAccuracy.High), cancellationToken);
                Console.WriteLine($"[GPS Logger] Long:{location.Longitude} Lat:{location.Latitude}");
                var result = await SendGpsLocationToServerAsync(location, cancellationToken);
                if (result.StatusCode != HttpStatusCode.OK)
                {
                    Console.WriteLine("[GPS Logger] web api endpoint did not return successful result, aborting...");
                    Console.WriteLine("[GPS Logger] Status Message: " + result.StatusDescription);
                    break;
                }
                await Task.Delay(delay, cancellationToken);
            }
        }

        /// <summary>
        /// Send the acquired location to the remote endpoint
        /// </summary>
        /// <param name="location">Location to send</param>
        /// <param name="cancellationToken">Token used to cancel the request</param>
        /// <returns>Response from the rest client.</returns>
        private async Task<IRestResponse> SendGpsLocationToServerAsync(Location location, CancellationToken cancellationToken)
        {
            //TODO: This ID should be user dependent.
            var id = "1";
            var gpsLocation = new GpsLocation
            {
                Altitude = location.Altitude,
                Course = location.Course,
                Latitude = location.Latitude,
                Longitude = location.Longitude,
                Speed = location.Speed,
                TimeStamp = location.Timestamp
            };
            var request = new RestRequest($"api/gps/{id}", Method.POST);
            request.AddJsonBody(gpsLocation);
            return await _client.ExecuteTaskAsync(request, cancellationToken);
        }
    }
}
