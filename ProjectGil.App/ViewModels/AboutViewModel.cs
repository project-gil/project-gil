﻿using ProjectGil.App.Services;
using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace ProjectGil.App.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            var gps = new GpsService();
            Title = "About";

            OpenWebCommand = null;
            
        }

        public ICommand OpenWebCommand { get; }
    }
}