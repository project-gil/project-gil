﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ProjectGil.Web.UITest {
    public class TestPageBase : IDisposable
    {
        private readonly string _uri;
        public IWebDriver WebDriver { get; }

        public TestPageBase(string uri)
        {
            _uri = uri;
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");
            WebDriver = new ChromeDriver(chromeOptions);
        }

        public void Navigate() => WebDriver.Navigate().GoToUrl(_uri);

        public void Dispose()
        {
            WebDriver.Quit();
            WebDriver.Dispose();
        }
    }
}
