﻿using OpenQA.Selenium;

namespace ProjectGil.Web.UITest.Pages {
    public class RegisterPage : TestPageBase {
        public RegisterPage() : base("https://localhost:5001/Identity/Account/Register") { }

        public IWebElement PageHeader => WebDriver.FindElement(By.XPath("/html/body/div/main/h1"));
        public IWebElement EmailField => WebDriver.FindElement(By.XPath("//*[@id=\"Input_Email\"]"));
        public IWebElement PasswordField => WebDriver.FindElement(By.XPath("//*[@id=\"Input_Password\"]"));
        public IWebElement ConfirmPasswordField => WebDriver.FindElement(By.XPath("//*[@id=\"Input_ConfirmPassword\"]"));
        public IWebElement RegisterButton => WebDriver.FindElement(By.XPath("/html/body/div/main/div/div[1]/form/button"));
        public IWebElement ErrorSummary => WebDriver.FindElement(By.XPath("/html/body/div/main/div/div[1]/form/div[1]"));
    }
}
