﻿using OpenQA.Selenium;

namespace ProjectGil.Web.UITest.Pages {
    public class HomePage : TestPageBase {
        public HomePage() : base("http://localhost:5000") { }

        public IWebElement PageHeader => WebDriver.FindElement(By.XPath("/html/body/div/main/div/h1"));
        public IWebElement WelcomeText => WebDriver.FindElement(By.XPath("/html/body/div/main/div/p"));
        public IWebElement WelcomeLink => WebDriver.FindElement(By.XPath("/html/body/div/main/div/p/a"));
    }
}
