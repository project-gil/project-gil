﻿using OpenQA.Selenium;

namespace ProjectGil.Web.UITest.Pages {
    public class LoginPage : TestPageBase {
        public LoginPage() : base("https://localhost:5001/Identity/Account/Login") { }

        public IWebElement PageHeader => WebDriver.FindElement(By.XPath("/html/body/div/main/h1"));
        public IWebElement EmailField => WebDriver.FindElement(By.XPath("//*[@id=\"Input_Email\"]"));
        public IWebElement PasswordField => WebDriver.FindElement(By.XPath("//*[@id=\"Input_Password\"]"));
        public IWebElement LoginButton => WebDriver.FindElement(By.XPath("//*[@id=\"account\"]/div[5]/button"));
        public IWebElement ErrorSummary => WebDriver.FindElement(By.XPath("//*[@id=\"account\"]/div[1]"));
    }
}
