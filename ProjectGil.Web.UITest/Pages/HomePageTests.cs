﻿using System;
using Xunit;

namespace ProjectGil.Web.UITest.Pages {
    public class HomePageTests : IDisposable {
        private readonly HomePage _page;

        public HomePageTests()
        {
            _page = new HomePage();
            _page.Navigate();
        }

        [Fact]
        public void Test_PageTitle()
        {
            Assert.Contains("Home page", _page.WebDriver.Title);
        }

        [Fact]
        public void Test_Home_Page_Header() {
            Assert.True(_page.PageHeader.Displayed);
            Assert.Equal("Welcome", _page.PageHeader.Text);
        }

        [Fact]
        public void Test_Welcome_Text() {
            Assert.True(_page.WelcomeText.Displayed);
            Assert.Contains("Learn about", _page.WelcomeText.Text);
            Assert.Contains("building Web apps with ASP.NET Core.", _page.WelcomeText.Text);
        }

        [Fact]
        public void Test_Welcome_Link() {
            Assert.True(_page.WelcomeLink.Displayed);
            Assert.Equal("https://docs.microsoft.com/aspnet/core", _page.WelcomeLink.GetAttribute("href"));
        }

        public void Dispose()
        {
            _page.Dispose();
        }
    }
}
