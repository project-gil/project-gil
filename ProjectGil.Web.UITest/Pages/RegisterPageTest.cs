﻿using System;
using System.Threading;
using OpenQA.Selenium;
using Xunit;

namespace ProjectGil.Web.UITest.Pages {
    public class RegisterPageTest : IDisposable {
        private readonly RegisterPage _page;

        public RegisterPageTest()
        {
            _page = new RegisterPage();
            _page.Navigate();
        }

        [Fact]
        public void Test_PageTitle()
        {
            Assert.Contains("Register", _page.WebDriver.Title);
        }

        [Fact]
        public void Test_Register_Page_Header() {
            Assert.True(_page.PageHeader.Displayed);
            Assert.Equal("Register", _page.PageHeader.Text);
        }

        [Fact]
        public void Test_RequiredFields() {
            //You have to double click to hit serverside validation.
            _page.RegisterButton.Click();
            _page.RegisterButton.Click();
            Assert.Contains("The Email field is required.", _page.ErrorSummary.Text);
            Assert.Contains("The Password field is required.", _page.ErrorSummary.Text);
        }

        [Fact]
        public void Test_EmailValidation() {
            _page.EmailField.SendKeys("somethingthatisnotanemail");
            //You have to double click to hit serverside validation.
            _page.RegisterButton.Click();
            _page.RegisterButton.Click();
            Assert.Contains("The Email field is not a valid e-mail address.", _page.ErrorSummary.Text);
        }

        [Fact]
        public void Test_PasswordNotMatchingValidation() {
            _page.PasswordField.SendKeys("Password1!");
            _page.ConfirmPasswordField.SendKeys("Password2!");
            //You have to double click to hit serverside validation.
            _page.RegisterButton.Click();
            _page.RegisterButton.Click();
            Assert.Contains("The password and confirmation password do not match.", _page.ErrorSummary.Text);
        }

        [Theory]
        [InlineData("testing", new [] {"Passwords must have at least one uppercase ('A'-'Z').", "Passwords must have at least one non alphanumeric character.", "Passwords must have at least one digit ('0'-'9')."})]
        [InlineData("testing1", new[] { "Passwords must have at least one uppercase ('A'-'Z').", "Passwords must have at least one non alphanumeric character."})]
        [InlineData("testing1!", new[] { "Passwords must have at least one uppercase ('A'-'Z')." })]
        public void Test_PasswordRequirements(string password, string[] expectedValidation) {
            _page.EmailField.SendKeys("test@test.com");
            _page.PasswordField.SendKeys(password);
            _page.ConfirmPasswordField.SendKeys(password);
            //You have to double click to hit serverside validation.
            _page.RegisterButton.Click();
            var text = _page.ErrorSummary.Text;

            var errorMessages = _page.ErrorSummary.FindElements(By.TagName("li"));
            Assert.Equal(expectedValidation.Length, errorMessages.Count);
            foreach (var messages in expectedValidation) {
                Assert.Contains(messages, _page.ErrorSummary.Text);
            }
        }

        public void Dispose()
        {
            _page.Dispose();
        }
    }
}
