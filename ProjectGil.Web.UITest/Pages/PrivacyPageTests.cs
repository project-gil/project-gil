﻿using System;
using Xunit;

namespace ProjectGil.Web.UITest.Pages {
    public class PrivacyPageTests : IDisposable {
        private readonly PrivacyPage _page;

        public PrivacyPageTests()
        {
            _page = new PrivacyPage();
            _page.Navigate();
        }

        [Fact]
        public void Test_PageTitle()
        {
            Assert.Contains("Privacy Policy", _page.WebDriver.Title);
        }

        [Fact]
        public void Test_Privacy_Policy_Header() {
            Assert.True(_page.PageHeader.Displayed);
            Assert.Equal("Privacy Policy", _page.PageHeader.Text);
        }

        [Fact]
        public void Test_Privacy_Policy_Text() {
            Assert.True(_page.PrivacyPolicy.Displayed);
            Assert.Equal("Use this page to detail your site's privacy policy.", _page.PrivacyPolicy.Text);
        }

        public void Dispose()
        {
            _page.Dispose();
        }
    }
}
