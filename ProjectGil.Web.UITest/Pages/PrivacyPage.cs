﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace ProjectGil.Web.UITest.Pages {
    public class PrivacyPage : TestPageBase {
        public PrivacyPage() : base("http://localhost:5000/Privacy") { }

        public IWebElement PageHeader => WebDriver.FindElement(By.XPath("/html/body/div/main/h1"));
        public IWebElement PrivacyPolicy => WebDriver.FindElement(By.XPath("/html/body/div/main/p"));
    }
}
