﻿using System;
using Xunit;

namespace ProjectGil.Web.UITest.Pages {
    public class LoginPageTest : IDisposable {
        private readonly LoginPage _page;

        public LoginPageTest()
        {
            _page = new LoginPage();
            _page.Navigate();
        }

        [Fact]
        public void Test_PageTitle()
        {
            Assert.Contains("Log in", _page.WebDriver.Title);
        }

        [Fact]
        public void Test_Register_Page_Header() {
            Assert.True(_page.PageHeader.Displayed);
            Assert.Equal("Log in", _page.PageHeader.Text);
        }

        [Fact]
        public void Test_RequiredFields() {
            _page.LoginButton.Click();
            Assert.Contains("The Email field is required.", _page.ErrorSummary.Text);
            Assert.Contains("The Password field is required.", _page.ErrorSummary.Text);
        }

        [Fact]
        public void Test_EmailValidation() {
            _page.EmailField.SendKeys("somethingthatisnotanemail");
            _page.LoginButton.Click();
            _page.LoginButton.Click();
            Assert.Contains("The Email field is not a valid e-mail address.", _page.ErrorSummary.Text);
        }

        public void Dispose()
        {
            _page.Dispose();
        }
    }
}
