﻿using System;
using OpenQA.Selenium.Chrome;
using Xunit;

namespace ProjectGil.Web.UITest.Integration {
    /// <summary>
    /// This class validates the account creation, validation, login, and removal
    /// </summary>
    public class AccountCreationAndRemovalTest : IDisposable {

        private readonly ChromeDriver _webDriver;

        public AccountCreationAndRemovalTest() {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");
            _webDriver = new ChromeDriver(chromeOptions);
        }


        [Theory]
        [InlineData("testing1@testing1.com", "Testing1!")]
        public void IntegrationTest(string email, string password) {
            //Register Account
            _webDriver.Navigate().GoToUrl("https://localhost:5001/Identity/Account/Register");
            _webDriver.FindElementByXPath("//*[@id=\"Input_Email\"]").SendKeys(email);
            _webDriver.FindElementByXPath("//*[@id=\"Input_Password\"]").SendKeys(password);
            _webDriver.FindElementByXPath("//*[@id=\"Input_ConfirmPassword\"]").SendKeys(password);
            _webDriver.FindElementByXPath("/html/body/div/main/div/div[1]/form/button").Click();

            //Confirm Email Page
            Assert.Equal("https://localhost:5001/Identity/Account/RegisterConfirmation?email=testing1@testing1.com", _webDriver.Url);
            _webDriver.FindElementByXPath("//*[@id=\"confirm-link\"]").Click();
            Assert.Contains("Thank you for confirming your email.", _webDriver.FindElementByXPath("/html/body/div/main/div").Text);

            //Confirm Account Taken
            _webDriver.Navigate().GoToUrl("https://localhost:5001/Identity/Account/Register");
            _webDriver.FindElementByXPath("//*[@id=\"Input_Email\"]").SendKeys(email);
            _webDriver.FindElementByXPath("//*[@id=\"Input_Password\"]").SendKeys(password);
            _webDriver.FindElementByXPath("//*[@id=\"Input_ConfirmPassword\"]").SendKeys(password);
            _webDriver.FindElementByXPath("/html/body/div/main/div/div[1]/form/button").Click();
            Assert.Contains($"User name '{email}' is already taken.",
                _webDriver.FindElementByXPath("/html/body/div/main/div/div[1]/form/div[1]").Text);

            //Login Page
            _webDriver.Navigate().GoToUrl("https://localhost:5001/Identity/Account/Login");
            _webDriver.FindElementByXPath("//*[@id=\"Input_Email\"]").SendKeys(email);
            _webDriver.FindElementByXPath("//*[@id=\"Input_Password\"]").SendKeys(password);
            _webDriver.FindElementByXPath("//*[@id=\"account\"]/div[5]/button").Click();
            Assert.Contains($"Hello {email}!", _webDriver.FindElementByXPath("/html/body/header/nav/div/div/ul/li[1]/a").Text);

            //Account Management Page
            _webDriver.Navigate().GoToUrl("https://localhost:5001/Identity/Account/Manage");
            _webDriver.FindElementByXPath("//*[@id=\"personal-data\"]").Click();
            _webDriver.FindElementByXPath("//*[@id=\"delete\"]").Click();
            Assert.Equal("Deleting this data will permanently remove your account, and this cannot be recovered.", _webDriver.FindElementByXPath("/html/body/div/main/div/div/div[2]/div[1]/p/strong").Text);
            _webDriver.FindElementByXPath("//*[@id=\"Input_Password\"]").SendKeys(password);
            _webDriver.FindElementByXPath("//*[@id=\"delete-user\"]/button").Click();

            //Try to login again with deleted account
            _webDriver.Navigate().GoToUrl("https://localhost:5001/Identity/Account/Login");
            _webDriver.FindElementByXPath("//*[@id=\"Input_Email\"]").SendKeys(email);
            _webDriver.FindElementByXPath("//*[@id=\"Input_Password\"]").SendKeys(password);
            _webDriver.FindElementByXPath("//*[@id=\"account\"]/div[5]/button").Click();
            Assert.Contains("Invalid login attempt.", _webDriver.FindElementByXPath("//*[@id=\"account\"]/div[1]").Text);
        }

        public void Dispose() {
            _webDriver?.Dispose();
        }
    }
}
